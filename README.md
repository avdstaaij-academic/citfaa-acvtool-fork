# ACVTool, with instrumentation blueprint output

This is a fork of [ACVTool](https://github.com/pilgun/acvtool) that outputs
blueprints as a side-effect of instrumentation. The additions are part of the
bachelor thesis "Composing instrumentation tools for Android apps", by Arthur
van der Staaij.

Visit the [original repository](https://github.com/pilgun/acvtool) for
information on how to install and use ACVTool. When an APK is instrumented using
this fork, a blueprint file with the `.blpr` extension is placed at the same
location as the `.pickle` file. The tool will print this location as well.

## License

NOTICE: This file has been modified by *Arthur van der Staaij* under compliance
with the Apache 2.0 license from the original work of *SnT, University of
Luxembourg*.

Modifications Copyright © 2022 Arthur van der Staaij

Copyright © 2018 SnT, University of Luxembourg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use the files under this repository except in compliance with
the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
